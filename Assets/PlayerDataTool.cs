﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

/// <summary>
/// Use: Delete the persistent data path and PlayerPrefs
/// Shortcut
///     Ctrl + Shift + Q
/// </summary>

static public class PlayerDataTool
{
	[MenuItem("Player Data/Delete Player Data %#q")]
	static void DeletePlayerPrefsAndPersistentDataPath()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		FileUtil.DeleteFileOrDirectory(Application.persistentDataPath);
		Debug.Log("All saved data has been deleted!");
	}
}
#endif