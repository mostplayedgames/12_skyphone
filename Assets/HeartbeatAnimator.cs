﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartbeatAnimator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AnimateIn ();
	}

	void AnimateIn()
	{
		LeanTween.cancel (this.gameObject);
		LeanTween.scale (this.gameObject, this.gameObject.transform.localScale + new Vector3 (0.15f, 0.15f, 0.15f), 0.2f).setLoopPingPong ().setLoopCount (2);
		LeanTween.delayedCall (2f, AnimateIn);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
