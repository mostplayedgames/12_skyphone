﻿using UnityEngine;
using System.Collections;

public class ZRotator : MonoBehaviour {

	public Vector3 m_rotateVector;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.localEulerAngles += m_rotateVector * Time.deltaTime;
	}
}
