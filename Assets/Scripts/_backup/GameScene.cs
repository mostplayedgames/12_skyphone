﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	SPLASH,
	START,
	IDLE,
	AIMING,
	SHOOT,
	RESULTS
}

public enum GAME_MODES
{
	CLASSIC,
	QUICK,
	ENDLESS,
	ENDLESS_QUICK
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectImage;
	public Image imageButtons;
	public GameObject objectPrice;
	public Sprite spriteImage;
	public GameObject topObject;
	public GameObject bottomObject;
	public RuntimeAnimatorController animator;
	public string m_textName;
	public GameObject m_objectCoin;
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public GameObject m_rootSpawnLocationLeft;
	public GameObject m_rootSpawnLocationRight;
	public GameObject m_rootSpawnLocationBottom;

	public MagnetObject m_scriptMagnetObject;

	public List<ShopBird> m_listShopBirds;
	//public List<ShopBird> m_listShopBalls;

	public GameObject m_objectWatchVideo;

	public List<Color> m_listBackgroundColor;


	//public GameObject m_objectCharHead;
	public GameObject m_objectParticle;
	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_prefabEnemy;
	public GameObject m_prefabStackObject;
	public GameObject m_prefabStackObjectPart;

	//public TextMesh m_textMeshScore;

	//public GameObject m_prefabArrow;
	//public GameObject m_prefabBird;
	//public GameObject m_prefabBirdBig;
	//public GameObject m_prefabBall;

	public GameObject m_objectTarget;
	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTopBar;
	public GameObject m_objectHitParticle;
	//public GameObject m_objectHitBow;
	public GameObject m_objectEarned;

	//public GameObject m_objectRootArrows;
	//public GameObject m_objectArrowMock;
	//public GameObject m_objectTulong;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;
	public GameObject m_objectFullBar;

	//public GameObject m_objectTrumpsHairHead;
	//public GameObject m_objectTrumpsHairBird;

	public GameObject m_objectHeadshot;

	public GameObject m_objectNormal;
	public GameObject m_objectWin;

	public GameObject m_objectSplash;
	public GameObject m_objectCameraObject;

	public GameObject m_objectTopBarObject;

	public List<GameObject> m_listButtons;
	public List<GameObject> m_listButtonsResults;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;

	public AudioClip m_audioThrow;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public Text m_textScore;
	public Text m_textLevel;
	public Text m_textTitle;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textModes2;
	public Text m_textBest;

	public GameObject m_objectTutorialHand3;

	public GameObject m_objectMagnet;

	public GameObject m_objectGameTitle;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public GameObject m_objectBall;
	public GameObject m_objectBallRoot;
	public GameObject m_objectShopButton;

	public GAME_STATE m_eState;
	GameObject m_currentEnemy;

	public List<GameObject> m_listEnemies;

	public List<GameObject> m_listLeftObjects;
	public List<GameObject> m_listRightObjects;

	public GameObject m_objectTutorialFinger;

	public GameObject m_objectCameraSizeObject;
	bool m_isCameraAnimating;

	public Text m_textBestHome;

	public GameObject m_objectLightObject;
	public List<Color> m_listLightColor;

	public GameObject m_objectRewardButton;

	public List<GameObject> m_listMouths;
	public List<GameObject> m_listEyes;

	int m_isWatchVideo;

	Vector3 m_startPosition;
	Vector2 m_positionTouch;

	int m_currentHighscore;
	int m_currentLevel;
	int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	public int m_currentMode;
	int m_currentBird;
	int m_currentPurchaseCount;
	int m_currentAttempts;

	float m_previousPosition = 0;

	float m_currentIncentifiedAdsTime;
	float m_currentDirectionTime;

	float m_currentLeftGroupPosition = -999;
	float m_currentRightGroupPosition = -999;

	bool m_isSuccess = false;

	string m_stringSuffix = "";

	bool m_animateIn = true;

	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Hit")) {
			PlayerPrefs.SetInt ("Hit", 0);
		}
		if (!PlayerPrefs.HasKey ("Mode")) {
			PlayerPrefs.SetInt ("Mode", 0);
		}
		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) {
			PlayerPrefs.SetInt ("currentBird", 0);
		}

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) {
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		for (int x = 0; x < 20; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 0);
				}
			}
		}

		for (int x = 0; x < 10; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 0);
				}
			}
		}

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;
	}

	void Start()
	{
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelQuick")) {
			PlayerPrefs.SetInt ("LevelQuick", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelPupu")) {
			PlayerPrefs.SetInt ("LevelPupu", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelEndless")) {
			PlayerPrefs.SetInt ("LevelEndless", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelCurve")) {
			PlayerPrefs.SetInt ("LevelCurve", 1);
		}

		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}

		ZPlatformCenterMgr.Instance.Login ();

		//ZObjectMgr.Instance.AddNewObject (m_prefabEnemy.name, 20, "");
		//ZObjectMgr.Instance.AddNewObject (m_prefabBall.name, 20, "");
		ZObjectMgr.Instance.AddNewObject (m_prefabStackObject.name, 100, "");
		//ZObjectMgr.Instance.AddNewObject (m_prefabStackObjectPart.name, 20, "");

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_currentBird = PlayerPrefs.GetInt ("currentBird");

		ShowSplash (true);
		m_isCameraAnimating = false;

		//m_objectTulong.SetActive (false);
		m_currentMode = 1;// PlayerPrefs.GetInt ("Mode");
		m_currentAttempts = PlayerPrefs.GetInt ("Attempts");
		//MTCharacter.instance.m_currentMode = m_currentMode;
		//m_currentLevel = 12;

		m_currentIncentifiedAdsTime = 60;
		m_currentDirectionTime = 0.1f;

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";

		m_objectWatchVideo.SetActive (false);

		//m_currentMode = 1;
		//m_currentLevel = 0;

		//LoadData ();
		//RandomizeColor();
		newColor = m_listLightColor [0];
		SetupLevel ();
		m_currentAds = 0;
		m_isWatchVideo = 0;

		ZAdsMgr.Instance.RequestInterstitial ();

	}

	public bool isSplash = false;

	void ShowSplash(bool fromStart = false)
	{
		isSplash = true;
		m_eState = GAME_STATE.SPLASH;

		Color splashColor = m_objectSplash.GetComponent<SpriteRenderer> ().color;
		if (fromStart) {
			ShowSplashRoutine2 ();
			m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (splashColor.r, splashColor.g, splashColor.b, 1);
		}
		else {
			LeanTween.delayedCall(0.15f, ShowSplashRoutine2);
			//ShowSplashRoutine2 ();
			LeanTween.alpha (m_objectSplash, 1, 0.1f).setOnComplete (ShowSplashRoutine);
		}


	}

	void ShowSplashRoutine2()
	{
		LeanTween.cancel (m_objectGameTitle);

		/*if (m_objectSplash.GetComponent<SpriteRenderer> ().color.a == 0)
		if( fromStart )
			m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (splashColor.r, splashColor.g, splashColor.b, 1);
		else
			LeanTween.alpha (m_objectSplash, 1, 0.5f).setOnComplete (ShowSplashRoutine);
		else
			m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (splashColor.r, splashColor.g, splashColor.b, 1);*/
		LeanTween.delayedCall (0.7f, StartRoutine);
		//m_objectGameTitle.transform.localPosition = new Vector3 (8.2f, 22.1f, -11.5f);
		//LeanTween.move (m_objectGameTitle, m_objectGameTitle.transform.localPosition - new Vector3 (0, 17, 0), 4f).setEase (LeanTweenType.easeOutBack);
		//LeanTween.move (m_objectGameTitle, new Vector3(8.2f, -10.5f, -11.5f), 3f).setEase (LeanTweenType.easeOutBack);

		//m_objectGameTitle.transform.localPosition = new Vector3 (-28.2f, -10.5f, -11.5f);
		m_objectGameTitle.transform.localPosition = new Vector3 (-10.2f, 2.5f, -11.5f);
		LeanTween.move (m_objectGameTitle, new Vector3 (8.2f, -10.5f, -11.5f), 0.5f);//.setEase (LeanTweenType.easeOutBack);

		//m_objectCameraObject.transform.eulerAngles = new Vector3 (15, -45f, 0);
		//LeanTween.rotateLocal (m_objectCameraObject, new Vector3 (30, -45f, 0), 4f);//.setEase(LeanTweenType.easeInOutExpo);
	}

	void ShowSplashRoutine()
	{
		SetupLevel ();
		m_eState = GAME_STATE.SPLASH;
		//ResetFace ();
	}

	void StartRoutine()
	{
		isSplash = false;
		m_eState = GAME_STATE.START;
		LeanTween.alpha (m_objectSplash, 0, 0.3f);

		LeanTween.delayedCall (0.5f, FinishSplashAnimation);

	}

	void FinishSplashAnimation()
	{
		hasNotFinishedAnimation = false;
	}

	void Update()
	{
		if (ZGameMgr.instance.isPause)
			return;
		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (m_objectShop.activeSelf) {
				m_objectShop.SetActive (false);
				ZAudioMgr.Instance.PlaySFX (m_audioButton);
			}
			else
				Application.Quit ();
		}

		if (m_isCameraAnimating) {
			m_objectCameraObject.GetComponent<Camera> ().orthographicSize = m_objectCameraSizeObject.transform.localPosition.x;
		}

		#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Space))
			ZRewardsMgr.instance.RewardDebug();
		#endif

		m_currentIncentifiedAdsTime -= Time.deltaTime;
	}

	public void Hit(Vector3 position)
	{
		m_objectHitParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectHitParticle.transform.position = position;
		m_objectHitParticle.SetActive (true);
		m_objectHitParticle.GetComponent<ParticleSystem> ().Play ();

		ZAudioMgr.Instance.PlaySFX (m_audioCatch);
	}

	public void HitHeadshot(Vector3 position)
	{
		m_objectParticlePerfect.GetComponent<ParticleSystem> ().Clear ();
		m_objectParticlePerfect.transform.position = position;
		m_objectParticlePerfect.SetActive (true);
		m_objectParticlePerfect.GetComponent<ParticleSystem> ().Play ();

		m_objectHeadshot.SetActive (true);
		LeanTween.scale (m_objectHeadshot, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);

		ZAudioMgr.Instance.PlaySFX (m_audioHeadshot);
	}

	void HitHeadshotRoutine()
	{
		m_objectHeadshot.SetActive (false);
	}

	public int GetBestScore()
	{
		return PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
	}
		
	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		//Debug.Log ("Die");

		if (m_currentScore <= 0) {
			m_textScore.text = "" + (m_currentScore);
			m_textTitle.text = "SkyPhone";
		}
		
		m_textScore.color = m_colorTextMiss;
		m_textTitle.color = m_colorTextMiss;
		m_objectMiss.SetActive (true);

		hasNotFinishedAnimation = true;

		//Time.timeScale = 0.4f;

		m_scriptMagnetObject.Die ();
		ResetFace ();
		//ZObjectMgr.Instance.ResetAll ();

		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentScore);

		//LeanTween.move (m_scriptMagnetObject.gameObject, m_scriptMagnetObject.gameObject.transform.localPosition + new Vector3 (0, 10, 0), 1f);

		int chatvalue = Random.Range (0, 9);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "BEST EVER!";
			break;
		case 1 : 
			m_textMiss.text = "REVOLUTIONARY!";
			break;
		case 2 : 
			m_textMiss.text = "ITS MAGICAL!";
			break;
		case 3 : 
			m_textMiss.text = "BEST EVAH!";
			break;
		case 4 : 
			m_textMiss.text = "FLAT DESIGN!";
			break;
		case 5 : 
			m_textMiss.text = "BEST EVER YET";
			break;
		case 6 : 
			m_textMiss.text = "MOST BEAUTIFUL!";
			break;
		case 7 : 
			m_textMiss.text = "LIKE MAGIC!";
			break;
		default : 
			m_textMiss.text = "MAGICAL EVER!";
			break;
		}



		LeanTween.cancel (m_textMiss.gameObject);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		ZCameraMgr.instance.DoShake ();
		ZAudioMgr.Instance.PlaySFX (m_audioDie);

		int randomMiss = Random.Range (0, 100);
		if( randomMiss > 80 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss);
		else if(  randomMiss > 70 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss1);
		else if(  randomMiss > 60 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss2);
		else if(  randomMiss > 50 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss3);
		else if(  randomMiss > 30 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss4);
		else
			ZAudioMgr.Instance.PlaySFX (m_audioMiss5);
	
		m_eState = GAME_STATE.RESULTS;

	
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		if (m_currentMode == 1 ){
			if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
				PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
				m_currentLevel = m_currentScore;
			}
		}

		m_textBest.text = "BEST " + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);
		m_textBestHome.text = "BEST " + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);

		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel - m_currentScore);

		LeanTween.delayedCall (1f, DieAnimateFirstRoutine);

		//if (m_currentAds > 2 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
		//	ZAdsMgr.Instance.ShowInsterstitial ();
		//	m_currentAds = 0;
			//LeanTween.delayedCall (1.75f, SetupLevel);
		//} else {
		//	ZAdsMgr.Instance.RequestInterstitial ();
		//}
		
		
		m_currentAds++;

		#if UNITY_ANDROID
		//if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[1].saveName), ZGameMgr.instance.m_listLeaderboard[1].leaderboardAndroid);
		//}
		#else
		//if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[1].saveName), ZGameMgr.instance.m_listLeaderboard[1].leaderboardIOS);
		//}
		#endif // UNITY_ANDROID

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

	}

	void DieAnimateFirstRoutine()
	{
		/*if (Advertisement.IsReady ("rewardedVideo") &&
			m_currentScore < m_currentLevel / 2 &&
			m_currentIncentifiedAdsTime <= 0 &&
			m_currentMode != 2 &&
			m_currentLevel > 5) {
			LeanTween.delayedCall (1f, ShowContinueRoutine);
		} else */if (m_currentLevel > 2 && m_currentAds > 5 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAds = 0;
			MoveCameraDown();

			LeanTween.delayedCall (0.5f, ShowUI);
		} else if (m_currentHits >= GetCurrentShopPrice () &&
			m_currentPurchaseCount < m_listShopBirds.Count - 1 &&
			ZRewardsMgr.instance.WillShowShopButton ()) 
		{

			ZRewardsMgr.instance.ShowShopButton ();

			MoveCameraDown();
			LeanTween.delayedCall (1f, ShowShopAvailable);
		} else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			MoveCameraDown();

			LeanTween.delayedCall(1f, ShowRewardSuccess);
			LeanTween.delayedCall(2.5f, ShowUI);
			//LeanTween.delayedCall (1f, ShowUI);
		} else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowReward ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			MoveCameraDown();

			LeanTween.delayedCall(1f, ShowReward);
			LeanTween.delayedCall(2.5f, ShowUI);
			//LeanTween.delayedCall (1f, ShowUI);
		} else{
			//LeanTween.delayedCall (1f, ShowUI);

			LeanTween.delayedCall(0.5f, ShowUI);
			MoveCameraDown();
		}
	}

	public int GetScore()
	{
		return m_currentScore;
	}

	void MoveCameraDown()
	{
		if( m_currentScore > 5 ){
			LeanTween.moveX (m_objectCameraSizeObject, 16f + (m_currentScore / 1f), 0.5f);//.setEase(LeanTweenType.easeInOutCubic);
			LeanTween.move (m_objectCameraObject, new Vector3 (14.1f, -5.6f + (m_currentScore/1.6f) - 1f, -20), 0.5f);
		}
		else{
			LeanTween.moveX (m_objectCameraSizeObject, 13.36f, 0.5f);//.setEase(LeanTweenType.easeInOutCubic);
			LeanTween.move (m_objectCameraObject, new Vector3 (14.1f, -7.6f + (m_currentScore/1.6f) - 1f, -20), 0.5f);
		}
		m_isCameraAnimating = true;
	}

	void ShowUI(){
		
		CameraAnimDone ();

		
		//m_objectCamera.gameObject.transform.localPosition = new Vector3 (14.1f, -4.6f, -20);
		
	}

	void CameraAnimDone()
	{
		LeanTween.cancel (m_objectCameraSizeObject);
		m_objectCameraSizeObject.transform.localPosition = new Vector3 (13.36f, -2f, 0);
		m_isCameraAnimating = false;
		m_rootSpawnLocationBottom.SetActive (true);
	foreach (GameObject obj in m_listButtonsResults) {
		obj.SetActive (true);
	}
	}

	void CameraAnimDone2(){
		m_isCameraAnimating = false;
	}

	public void Retry()
	{
		//SetBGColor ();
		RandomizeColor();
	foreach (GameObject btn in m_listButtonsResults) {
		btn.gameObject.SetActive (false);
	}
		m_objectRewardButton.SetActive (false);
		m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (newColor.r,
			newColor.g,
			newColor.b,
			0);
		ShowSplash ();
		
		m_rootSpawnLocationBottom.SetActive (false);

		//ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable(){
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	LeanTween.delayedCall (2.505f, ShowUI);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowReward(){
		//LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowRewardSuccess(){
		//LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowRewardSuccess ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void DieForce()
	{
	}

	public void Score(int score)
	{
		if (m_eState == GAME_STATE.START)
			return;
		
		if (m_currentMode == 1) {
			m_currentScore += score;
			m_textScore.text = "" + (m_currentScore);
			m_textTitle.text = "SkyPhone";
			m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));
			m_eState = GAME_STATE.IDLE;

			m_isCameraAnimating = true;
			//LeanTween.move (m_objectCameraObject, new Vector3 (14.1f, -5.6f + (m_currentScore/1.6f), -20), 1f);
		//LeanTween.moveX (m_objectCameraSizeObject, 11f + (m_currentScore*0.5f), 0.2f).setOnComplete(CameraAnimDone2);//.setEase(LeanTweenType.easeInOutCubic);

			//m_scriptMagnetObject.MoveArm ();
			//m_textMeshScore.text = m_currentScore + "";
			//LeanTween.delayedCall (0.01f, ScoreRoutine);


		} else {
			if (m_currentScore <= 0)
				return;
		
			m_currentScore -= score;
			if (m_currentScore <= 0)
				m_currentScore = 0;
		
			m_textScore.text = "" + m_currentScore;


			Debug.Log ("Score : " + m_currentScore);
			if (m_currentScore <= 0) {
				Success ();
			} else if (m_currentMode == 1) {
			m_eState = GAME_STATE.IDLE;
			} else {
				//LeanTween.delayedCall (0.5f, ScoreRoutine);
				//SpawnEnemyMain ();
				m_eState = GAME_STATE.IDLE;
			}
		}

		AddCoins (score);
		ResetFace ();
	
		//LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel);
		ZAnalytics.Instance.SendLevelComplete ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		m_textScore.color = m_colorTextSuccess;
		m_textTitle.color = m_colorTextSuccess;
		m_textLevel.color = m_colorTextSuccess;
		ZAudioMgr.Instance.PlaySFX (m_audioDie);

		m_objectSuccess.SetActive (true);
		LeanTween.cancel (m_objectSuccess.gameObject);
		LeanTween.scale (m_objectSuccess.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		m_objectParticle.SetActive (true);
		LeanTween.delayedCall (3.5f, SuccessRoutine);
		m_eState = GAME_STATE.RESULTS;

		//float defaultScale = MTCharacter.instance.m_defaultScale;
		//LeanTween.scale (MTCharacter.instance.gameObject, new Vector3(defaultScale+0.4f, defaultScale+0.4f,defaultScale+0.4f ), 0.2f);

		m_objectNormal.SetActive (false);
		m_objectWin.SetActive (true);

		//ZObjectMgr.Instance.ResetAll ();

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		//LeanTween.moveLocalY (m_objectHitBow, m_objectHitBow.transform.localPosition.y + 2, 0.5f).setLoopCount (6).setLoopPingPong();

		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);

		ZAnalytics.Instance.SendCurrentCoins (m_currentHits);
		//LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);

			
		m_currentAds++;

		#if UNITY_ANDROID

		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else

		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

	
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;
		
		//if( isRateUs > 0 )
		//	return;
		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			//m_currentAds = 0;
		} else {
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			//if (rewardedSource == 0)
				ContinueLevel ();
			//if (rewardedSource == 1) {
			//	AddCoins (100);
			//	m_objectEarned.SetActive (true);
				//new MobileNativeMessage("You get 100 Eagles", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			//}
			//m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		//m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		//if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			//ZRewardsMgr.instance.ShowRewardSuccess ();
		//}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();
		//PlayerPrefs.SetInt ("Level", m_currentLevel);

		/*if( m_currentMode == 0 )
			PlayerPrefs.SetInt ("Level", m_currentLevel);
		else if( m_currentMode == 1 )
			PlayerPrefs.SetInt ("LevelQuick", m_currentLevel);
		else if( m_currentMode == 2 )
			PlayerPrefs.SetInt ("LevelEndless", m_currentLevel);*/

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		/*if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
			LeanTween.delayedCall (1f, SetupLevel);
		} else {*/
			SetupLevel ();
		//}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ScoreRoutine()
	{
		//if( m_currentMode != 1)
		//	SpawnEnemy ();
	}

	public int GetLevel(){
		return m_currentLevel;
		//return 10;
	}

	public void SpawnPart()
	{
		m_scriptMagnetObject.SpawnPart ();
	}

	bool hasNotFinishedAnimation = false;

	public void DropMagnet()
	{
		if (m_eState == GAME_STATE.SPLASH)
			return;
		if (m_eState == GAME_STATE.RESULTS)
			return;
		if (hasNotFinishedAnimation)
			return;

		if (m_eState == GAME_STATE.START) {
			//m_textScore.text = "" + (m_currentScore + 1);
			//m_textTitle.text = "SkyFone";
		LeanTween.moveLocal(m_objectTopBarObject, m_objectTopBarObject.transform.localPosition + new Vector3(0,120,0), 1f);
		LeanTween.move (m_objectGameTitle, m_objectGameTitle.transform.localPosition - new Vector3 (100f, 0, 0), 1f).setEase(LeanTweenType.easeInOutCubic);
			m_objectShopButton.SetActive (false);
		}
		m_scriptMagnetObject.Drop ();
	m_textBestHome.gameObject.SetActive (false);
	m_objectTutorialHand3.SetActive (false);
		m_eState = GAME_STATE.IDLE;
		//m_scriptMagnetObject.m_textMeshNumber.gameObject.SetActive (true);
		//m_textMeshScore.gameObject.SetActive (true);
	}
		
	public void Play(int mode)
	{	
		m_currentMode = mode;

		/*m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (m_listBackgroundColor [mode + 1].r, 
																		m_listBackgroundColor [mode + 1].g,
																		m_listBackgroundColor [mode + 1].b,
																		0);
		LeanTween.alpha (m_objectSplash, 1, 0.5f);*/
		//m_textMeshScore.gameObject.SetActive (true);

		SetupLevel ();

		if (m_currentMode == 1) {
			m_currentScore = 0;
			m_textScore.text = "" + m_currentScore;
			m_currentHighscore = m_currentLevel;
		}

		
		if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 0) {
			LeanTween.alpha (m_objectTutorialFinger, 1, 0.2f);
		}
			
		m_eState = GAME_STATE.IDLE;
		m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		m_textTitle.color = m_colorTextCorrect;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		//m_objectArrowMock.SetActive (true);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

		m_objectTutorial.GetComponent<Text> ().text = "HOLD TO AIM\n RELEASE TO DROP";

		if (m_animateIn) {
			foreach (GameObject btn in m_listButtons) {
				btn.gameObject.SetActive (false);
			}

			Debug.Log ("Animate");

			foreach (GameObject btn in m_listLeftObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition - 150, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			foreach (GameObject btn in m_listRightObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
			LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition + 150, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			m_objectWatchVideo.SetActive (false);
			m_animateIn = false;
		}

		m_eState = GAME_STATE.IDLE;

		//m_textScore.color = m_colorTextCorrect;
		//m_objectArrowMock.SetActive (true);

	}

	public void ChangeMode()
	{
		m_currentMode++;
		if (m_currentMode >= ZGameMgr.instance.m_listLeaderboard.Count) {
			m_currentMode = 0;
		}

		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";

	
		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ResetModeText()
	{
		if (m_currentMode == 0)
			m_textLevel.text = "Level";
		else if (m_currentMode == 1)
			m_textLevel.text = "Highscore";
		else if (m_currentMode == 2)
			m_textLevel.text = "Highscore";
		else
			m_textLevel.text = "Level";
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		ScreenCapture.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		//		ShareScreenshotWithText ("");
		ZFollowUsMgr.Instance.Share();
		ZAnalytics.Instance.SendShareButton ();
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		//ZPlatformCenterMgr.Instance.Login ();
		//return;
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.PostScore(10);
		//return;

		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;

	public void ShowShop()
	{
		//ZGameMgr.instance.ShowScene ("ShopScene");
		m_objectShop.SetActive(true);
		ShopScene.instance.SetupScene();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		
		ZAnalytics.Instance.SendShopButton ();
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectBar;

	public void AddCoins(int index)
	{
		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	//PlayerPrefs.SetInt ("Hit", 0);
		//PlayerPrefs.Save ();
	
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

	m_objectBar.transform.localScale = new Vector3 (0.74f * (Mathf.Min(1f, m_currentHits*1.0f/GetCurrentShopPrice())), 1.19f, 0.99f);

		if (m_currentHits * 1.0f / GetCurrentShopPrice () >= 1f) {
			m_objectFullBar.SetActive (true);
		} else {
			m_objectFullBar.SetActive (false);
		}
		/*if (m_currentHits >= GetCurrentShopPrice () && m_eState == GAME_STATE.START && m_currentPurchaseCount < m_listShopBirds.Count) {
			LeanTween.delayedCall (0.2f, AddCoinRoutine);
		}*/
	}

		public void UpdateCoinBar()
	{
		m_objectBar.transform.localScale = new Vector3 (0.74f * (Mathf.Min (1f, m_currentHits * 1.0f / GetCurrentShopPrice ())), 1.19f, 0.99f);

		if (m_currentHits * 1.0f / GetCurrentShopPrice () >= 1f) {
			m_objectFullBar.SetActive (true);
		} else {
			m_objectFullBar.SetActive (false);
		}
	}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		//return;

		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public List<GameObject> m_listBirds;



	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0) {
			m_currentBird = count;
			m_objectShop.SetActive (false);
			PlayerPrefs.SetInt ("currentBird", m_currentBird);
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);


			for (int x = 0; x < 10; x++) {
				if (x == PlayerPrefs.GetInt ("currentBird") ) {
					GameScene.instance.m_listShopBirds [x].topObject.SetActive (true);
				GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (true);
				} else {
					GameScene.instance.m_listShopBirds [x].topObject.SetActive (false);
					GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (false);
				}
			}

			m_objectReadyUnlock.SetActive (false);

		} else {
			BuyBird (count);
			ZAudioMgr.Instance.PlaySFX (m_audioBuy);
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}

	public void BuyBird(int count)
	{
		if (m_currentHits >= m_listPrices[m_currentPurchaseCount] && PlayerPrefs.GetInt("birdbought" + count) == 0) {
			m_currentHits -= m_listPrices[m_currentPurchaseCount];
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("birdbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);
			
			m_currentPurchaseCount++;
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();
		//UseBird (count);

			//UseBird(count);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);

			UpdateCoinBar ();

			for (int x = 0; x < 10; x++) {
				if (x ==count ) {
					GameScene.instance.m_listShopBirds [x].topObject.SetActive (true);
					GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (true);
				} else {
					GameScene.instance.m_listShopBirds [x].topObject.SetActive (false);
					GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (false);
				}
			}

			ShopScene.instance.SetupScene (true, true);
		}
	}

	public void BuyBall(int count)
	{
		if (m_currentHits >= 250  && PlayerPrefs.GetInt("ballbought" + count) == 0) {
			m_currentHits -= 250;
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("ballbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("ballbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("ball", m_currentPurchaseCount);

			//m_currentPurchaseCount++;
			//PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			//PlayerPrefs.Save ();

			//UseBird(count);
			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	//ZAudioMgr.Instance.PlaySFX (m_audioButton);randomVerticalPosition

			ShopScene.instance.SetupScene ();
		}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}


	void RefreshMode()
	{	
		m_currentLevel = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		//m_currentScore = 0;

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";
	
		m_objectTutorial.GetComponent<Text> ().text = "TAP TO PLAY";
		ResetScore ();
	}

	Color newColor;

	public void RandomizeColor(){
		newColor = m_listLightColor [Random.Range (0, m_listLightColor.Count)];
		//m_objectLightObject.GetComponent<Light> ().color = newColor;
		//m_objectCameraObject.GetComponent<Camera>().backgroundColor = newColor;
		//m_objectSplash.GetComponent<SpriteRenderer> ().color = newColor;
	}

	public void SetBGColor(Color _newColor){
		//Color newColor = m_listLightColor [Random.Range (0, m_listLightColor.Count)];
		m_objectLightObject.GetComponent<Light> ().color = _newColor;
		m_objectCameraObject.GetComponent<Camera>().backgroundColor = _newColor;
		m_objectSplash.GetComponent<SpriteRenderer> ().color = _newColor;
	}

	void ResetFace()
	{
		int eyeIndex = Random.Range (0, m_listEyes.Count);
		int mouthIndex = Random.Range (0, m_listMouths.Count);

		int y = 99;
		if (Random.Range (0, 100) > 90)
			y = 99;
		foreach (GameObject obj in m_listEyes) {
			if (y == eyeIndex) {
				obj.SetActive (true);
			} else {
				obj.SetActive (false);
			}
			y++;
		}

		y = 99;
		if (Random.Range (0, 100) > 90)
			y = 99;
		foreach (GameObject obj in m_listMouths) {
			if (y == mouthIndex) {
				obj.SetActive (true);
			} else {
				obj.SetActive (false);
			}
			y++;
		}
	}

	void SetupLevel()
	{
		
		ZObjectMgr.Instance.ResetAll ();

		m_objectNoInternet.SetActive (false);
		m_objectShopButton.SetActive (true);
		m_objectTutorialHand3.SetActive (true);
	Time.timeScale = 1f;

		m_objectEarned.SetActive (false);

		RefreshMode ();
		ResetModeText ();
		m_objectCameraObject.GetComponent<Camera> ().orthographicSize = 13.36f;
		//m_textScore.text = "";
		//m_textMeshScore.gameObject.SetActive (false);
		//m_textMeshScore.text = "0";

		LeanTween.cancel (m_objectTopBarObject);
	m_objectTopBarObject.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (0, -17f, 369f);

		SetBGColor (newColor);
		
		m_rootSpawnLocationBottom.SetActive (false);

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
	m_textTitle.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		//m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);
		//Debug.LogError ("Setup Level");
		m_objectHeadshot.SetActive (false);
		m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);
	//m_objectTutorialFinger.SetActive (false);

		if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 1) {
			LeanTween.alpha (m_objectTutorialFinger, 0, 0.2f);
		}

		foreach (GameObject obj in m_listButtonsResults) {
			obj.SetActive (false);
		}


		m_objectTutorial.SetActive (true);
		m_objectTopBar.SetActive(true);
		//m_objectEarned.SetActive (false);

		m_objectNormal.SetActive (true);
		m_objectWin.SetActive (false);

		//ZAudioMgr.Instance.PlaySFX (m_audioButton);

		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (true);
		}

		if(m_currentMode == 3 )
			m_isSuccess = false;

		m_eState = GAME_STATE.START;

		m_scriptMagnetObject.Reset ();

		foreach (GameObject obj in m_listEyes) {
			obj.SetActive (false);
		}

		foreach (GameObject obj in m_listMouths) {
			obj.SetActive (false);
		}


		if (m_currentLeftGroupPosition == -999) {
			m_currentLeftGroupPosition = m_listLeftObjects [0].transform.localPosition.x;
		}
		if (m_currentRightGroupPosition == -999) {
			m_currentRightGroupPosition = m_listRightObjects [0].transform.localPosition.x;
		}

		if (!m_animateIn) {
			foreach (GameObject btn in m_listButtons) {
				btn.gameObject.SetActive (true);
			}

			

			foreach (GameObject btn in m_listLeftObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			foreach (GameObject btn in m_listRightObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			//m_objectWatchVideo.SetActive (false);
			m_animateIn = true;
		}

		//LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);
		//m_objectTrumpsHairBird.transform.localScale = Vector3.zero;

		//SpawnBall ();
		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		//m_scriptMagnetObject.SpawnPart ();
	m_textBest.text = "";
	m_textBestHome.text = "BEST " + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);
		m_textBestHome.gameObject.SetActive (true);

	m_objectBar.transform.localScale = new Vector3 (0.74f * (Mathf.Min(1f, m_currentHits*1.0f/GetCurrentShopPrice())), 1.19f, 0.99f);
		if (m_currentHits * 1.0f / GetCurrentShopPrice () >= 1f) {
		m_objectFullBar.SetActive (true);
		} else {
		m_objectFullBar.SetActive (false);
		}

		PlayerPrefs.Save ();
	}

	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		//m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		//Debug.LogError ("Setup Level");

		m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);

		m_objectTutorial.SetActive (true);
		//MTCharacter.instance.m_currentState = MTCHAR_STATES.IDLE;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_animateIn = true;
		m_eState = GAME_STATE.START;
		//SpawnBall ();
	}

	public void NextLevel(){
		m_scriptMagnetObject.NextLevel ();

		//Score (1);
	}

	void ResetScore()
	{
		m_textLevel.text = "Level";
		if (m_currentMode == 1) {
			m_currentScore = 0;
		} else {
			m_currentScore = m_currentLevel;
		}
		m_textScore.text = "";//	"SkyPhone " + (m_currentScore+1);
		m_textTitle.text = "";

	}

}
