﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopScene : ZGameScene {

	public static ShopScene instance;

	public Color m_colorBird;
	public Color m_colorNotbought;
	public Color m_colorBalls;
	public Color m_colorNotboughBalls;
	public Color m_colorTextColorBought;

	public AudioClip m_audioButton;


	public List<Text> m_listText;
	public List<Text> m_listTextBalls;
	public GameObject m_objectUnlockNow;
	public List<GameObject> m_listObjectShadow;

	public RectTransform m_rectTransformContent;

	void Awake()
	{
		instance = this;
	}

	public void Back()
	{
		this.gameObject.SetActive (false);
		GameScene.instance.m_objectReadyUnlock.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void SetupScene(bool isNormal = true, bool isUnlock = false)
	{
		//if( !isUnlock )
		//m_rectTransformContent.localPosition = new Vector3 (0, -30, 0);

		for (int x = 0; x < 10; x++) {
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {
				//GameScene.instance.m_listShopBirds [x].objectImage.SetActive (true);
				GameScene.instance.m_listShopBirds [x].imageButtons.GetComponent<Button> ().interactable = true;
				GameScene.instance.m_listShopBirds [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,1);
				GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorBird;
				GameScene.instance.m_listShopBirds [x].objectPrice.GetComponent<Text> ().color = m_colorTextColorBought;
				GameScene.instance.m_listShopBirds [x].m_objectCoin.SetActive (false);
				//GameScene.instance.m_listShopBirds [x].topObject.SetActive (true);
				//GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (true);
				//m_listObjectShadow [x].SetActive (true);
			} else {
				//GameScene.instance.m_listShopBirds [x].objectImage.SetActive (false);
				GameScene.instance.m_listShopBirds [x].imageButtons.GetComponent<Button> ().interactable = true;

				GameScene.instance.m_listShopBirds [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,0);
				GameScene.instance.m_listShopBirds [x].imageButtons.color = m_colorNotbought;
				GameScene.instance.m_listShopBirds [x].objectPrice.SetActive (true);
				GameScene.instance.m_listShopBirds [x].objectPrice.GetComponent<Text> ().color = new Color (1, 1, 1, 1f);
				GameScene.instance.m_listShopBirds [x].m_objectCoin.SetActive (true);
				//GameScene.instance.m_listShopBirds [x].topObject.SetActive (false);
				//GameScene.instance.m_listShopBirds [x].bottomObject.SetActive (false);
				//m_listObjectShadow [x].SetActive (false);
			}
		}

		/*for (int x = 0; x < 10; x++) {
			if (PlayerPrefs.GetInt ("ballbought" + x) > 0) {
				//GameScene.instance.m_listShopBalls [x].objectImage.SetActive (true);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (0, 0, 0, 0.2f);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (false);
			} else {
				GameScene.instance.m_listShopBalls [x].objectImage.SetActive (false);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,1);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorNotboughBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (true);
			}
		}*/

		int y = 0;
		foreach (Text txt in m_listText) {
			if (PlayerPrefs.GetInt ("birdbought" + y) > 0)
				txt.text = "";//GameScene.instance.m_listShopBirds [y].m_textName;
			else
				txt.text = GameScene.instance.GetCurrentShopPrice () + "";
				
			y++;
		}

		foreach (Text txt in m_listTextBalls) {
			txt.text = 250 + "";
		}

		if (isNormal) {
			m_objectUnlockNow.SetActive (false);
		} else {
			m_objectUnlockNow.SetActive (true);
		}
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
