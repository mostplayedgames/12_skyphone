﻿using UnityEngine;
using System.Collections;

public class PauseScene : ZGameScene {

	public static PauseScene instance;

	void Awake()
	{
		instance = this;
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
			ZGameMgr.instance.ShowScene ("GameScene");
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
