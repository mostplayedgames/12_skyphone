﻿using UnityEngine;
using System.Collections;

public class ZWaterMover : MonoBehaviour {

	float currentSpeed = 1;
	// Use this for initialization
	void Start () {
		currentSpeed = 0.4f;//Random.Range (0.4f, 0.6f);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.localPosition += new Vector3 (currentSpeed, 0, 0) * Time.deltaTime;

		if (this.transform.localPosition.x >= 15f) {
			this.transform.localPosition -= new Vector3(25, 0, 0);
		}
	}
}
