﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent (typeof(Button))]
[RequireComponent (typeof(EventTrigger))]
[ExecuteInEditMode]
public class ZButton : MonoBehaviour, IPointerDownHandler {
	
	public AudioClip m_audioSFX;
	
	ZGameScene m_buttonRootScene;
	
	// Use this for initialization
	void Start () 
	{
		m_buttonRootScene = this.transform.root.GetComponent<ZGameScene>();	
		this.GetComponent<Button>().onClick.AddListener(delegate {m_buttonRootScene.OnButtonUp(this); });
	}
	
	public void OnPointerDown(PointerEventData data)
	{
		m_buttonRootScene.OnButtonDown (this);
		if( m_audioSFX != null )
		{
			ZAudioMgr.Instance.PlaySFX (m_audioSFX);
		}
	}
}