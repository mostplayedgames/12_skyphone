﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StackObject : MonoBehaviour {

	public bool isAlive = false;
	//public bool isCurrentLevel = false;
	// Use this for initialization
	public List<GameObject> m_listScreen;
	public List<Texture> m_listScreenTextures;

	//public Material m_materialIcon1;
	//public Material m_materialIcon2;
	//public Material m_materialIcon3;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset()
	{
		m_listScreen[0].GetComponent<Renderer>().material.mainTexture = m_listScreenTextures[Random.Range(0, m_listScreenTextures.Count)];	
		m_listScreen[1].GetComponent<Renderer>().material.mainTexture = m_listScreenTextures[Random.Range(0, m_listScreenTextures.Count)];	
		m_listScreen[2].GetComponent<Renderer>().material.mainTexture = m_listScreenTextures[Random.Range(0, m_listScreenTextures.Count)];	
	}

	void OnCollisionEnter(Collision collision) {
		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		
		if (collision.gameObject.tag == "Die") {
			//if (collision.gameObject){// &&
			    //collision.gameObject.GetComponent<StackObject> () &&
			    //collision.gameObject.GetComponent<StackObject> ().isAlive) {
				GameScene.instance.Die ();
			//this.GetComponent<Rigidbody> ().AddForce (new Vector3 (Random.Range (400, 100), Random.Range (400, 100), Random.Range (400, 100)));
			//this.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range (400, 100), Random.Range (400, 100), Random.Range (400, 100)));

				//if( this.GetComponent<Rigidbody>() )
				//this.GetComponent<Rigidbody> ().isKinematic = true;
				//Debug.LogError (" Name : " + collision.gameObject.name);
				//Debug.LogError ("This Name : " + this.gameObject.name);
			//}


			/*if (isAlive) {
			GameScene.instance.SpawnPart ();
			this.GetComponent<Rigidbody> ().isKinematic = true;
			isAlive = false;
			//isCurrentLevel = true;
		}*/




		} else if ( isAlive ) {
			//GameScene.instance.SpawnPart ();
			if( this.GetComponent<Rigidbody>() )
				this.GetComponent<Rigidbody> ().isKinematic = true;
			//this.GetComponent<StackObject> ().enabled = false;
			isAlive = false;
			//GameScene.instance.Score (1);

			GameScene.instance.NextLevel ();

			//Debug.LogError ("Land");

		}

		//Debug.Log ("COllide with : " + this.gameObject.name);

	}
}
