﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum MTENEMY_STATES
{
	IDLE,
	RUN,
	RUNFAST,
	DIE,
}

public enum MTENEMY_ANIM_STATES
{
	RUNNING,
	SKATEBOARD,
	UMBRELLA,
	PLACKARD,
	BIRD,
	BOSS,
	SKATEBOARD_UMBRELLA
}

public class MTEnemy : MonoBehaviour {

	public AudioClip m_audioDie;

	public TextMesh m_textChant; 

	public MTENEMY_STATES m_currentState;
	public float m_moveSpeedMultiplier = 1;
	public float m_moveSpeedMultiplierVertical = 0;
	float m_moveSpeed = 2;
	public float m_moveDirection = 1;
	public float m_currentScale = 1;

	public SpriteRenderer m_rendererImage;

	public int m_currentMode = 0;

	public GameObject m_objectUmbrella;
	public GameObject m_objectSkateboard;
	public GameObject m_objectPlackard;
	public GameObject m_objectBird;

	public GameObject m_objectHair;

	int m_currentHP = 0;

	// Use this for initialization
	void Start () {
		//m_moveDirection = 1;

		m_currentState = MTENEMY_STATES.RUN;
		//LeanTween.scale (this.gameObject, new Vector3 (1, 0.8f, 1), 0.5f).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}
	
	// Update is called once per frame
	void Update () {
		switch (m_currentState) {
		case MTENEMY_STATES.IDLE:
			break;
		case MTENEMY_STATES.RUN:
			if (m_moveDirection == 1)
				MoveLeft ();
			else
				MoveRight ();

			VerticalLimiter ();
			break;
		case MTENEMY_STATES.RUNFAST:
			if (m_moveDirection == 1)
				MoveLeft ();
			else
				MoveRight ();

			VerticalLimiter ();
			break;
		case MTENEMY_STATES.DIE:
			break;
		}
	}

	public void SetAnimState(MTENEMY_ANIM_STATES state)
	{
		switch (state) {
		case MTENEMY_ANIM_STATES.RUNNING:
			m_objectUmbrella.SetActive (false);
			m_objectSkateboard.SetActive (false);
			m_objectPlackard.SetActive (false);
			m_objectBird.SetActive (false);
			break;
		case MTENEMY_ANIM_STATES.SKATEBOARD:
			m_objectUmbrella.SetActive (false);
			m_objectSkateboard.SetActive (true);
			m_objectPlackard.SetActive (false);
			m_objectBird.SetActive (false);
			break;
		case MTENEMY_ANIM_STATES.UMBRELLA:
			m_objectUmbrella.SetActive (false);
			m_objectSkateboard.SetActive (false);
			m_objectPlackard.SetActive (true);
			m_objectBird.SetActive (false);
			break;
		case MTENEMY_ANIM_STATES.BIRD:
			m_objectUmbrella.SetActive (false);
			m_objectSkateboard.SetActive (false);
			m_objectPlackard.SetActive (false);
			m_objectBird.SetActive (true);
			break;
		case MTENEMY_ANIM_STATES.SKATEBOARD_UMBRELLA:
			m_objectUmbrella.SetActive (true);
			m_objectSkateboard.SetActive (true);
			break;
		case MTENEMY_ANIM_STATES.BOSS:
			break;
		}
	}

	public void SetPlackard(bool visible)
	{
		if (visible) {
			m_objectPlackard.SetActive (true);
		} else {
			m_objectPlackard.SetActive (false);
		}
	}

	public void SetGriphon(bool visible)
	{
		if (visible) {
			m_objectBird.SetActive (true);
			m_objectSkateboard.SetActive (false);
		} else {
			m_objectBird.SetActive (false);
		}
	}

	public void SetMinMaxScale(float minScale, float maxScale)
	{
		float randomScale = Random.Range (minScale, maxScale);
		this.transform.localScale = new Vector3 (randomScale, randomScale, randomScale);
		m_currentScale = randomScale;
	}

	public void SetChant(int currentbird)
	{
		int chatvalue = Random.Range (0, 5);
		if(currentbird == 0 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Animo!";
				break;
			case 1 : 
				m_textChant.text = "Go Go Green!";
				break;
			case 2 : 
				m_textChant.text = "Fight!";
				break;
			default : 
				m_textChant.text = "Animo!";
				break;
			}
		}
		else if(currentbird == 7 ) { // UP
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Fight!";
				break;
			case 1 : 
				m_textChant.text = "Shout and Cheer!";
				break;
			case 2 : 
				m_textChant.text = "Hey hey!";
				break;
			default : 
				m_textChant.text = "Maroons!";
				break;
			}
		}
		else if(currentbird == 8 ) { // UST
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Fight!";
				break;
			case 1 : 
				m_textChant.text = "Go Fight!";
				break;
			case 2 : 
				m_textChant.text = "Victory!";
				break;
			default : 
				m_textChant.text = "Charge!";
				break;
			}
		}
		else if(currentbird == 3 ) { // Tamarraw
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Tamaraw!";
				break;
			case 1 : 
				m_textChant.text = "Animo Cheers!";
				break;
			case 2 : 
				m_textChant.text = "Tamaraw!";
				break;
			default : 
				m_textChant.text = "Power!";
				break;
			}
		}
		else if(currentbird == 6 ) { // Warriors
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Fight";
				break;
			case 1 : 
				m_textChant.text = "Warriors!";
				break;
			case 2 : 
				m_textChant.text = "Go Go Go!";
				break;
			default : 
				m_textChant.text = "Go Fight Red and White!";
				break;
			}
		}
		else if(currentbird == 2 ) { // Bulldogs
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Go Go";
				break;
			case 1 : 
				m_textChant.text = "Cheer!";
				break;
			case 2 : 
				m_textChant.text = "Lets Go!";
				break;
			default : 
				m_textChant.text = "Bulldogs!";
				break;
			}
		}
		else if(currentbird == 1 ) { // ADU
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Go Go";
				break;
			case 1 : 
				m_textChant.text = "Soar High!";
				break;
			case 2 : 
				m_textChant.text = "Falcons!";
				break;
			default : 
				m_textChant.text = "Lets go win this fight!";
				break;
			}
		}
		else if(currentbird == 4 ) { // HUGOT
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Bitter is Better";
				break;
			case 1 : 
				m_textChant.text = "Dont worry im strong";
				break;
			case 2 : 
				m_textChant.text = "#hugot!";
				break;
			default : 
				m_textChant.text = "Its Complicated";
				break;
			}
		}
		else if(currentbird == 5 ) { // Lions
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Fight";
				break;
			case 1 : 
				m_textChant.text = "Animo";
				break;
			case 2 : 
				m_textChant.text = "Go Fight";
				break;
			default : 
				m_textChant.text = "Lions!";
				break;
			}
		}
	}

	public void RunFast()
	{
		if (m_currentState == MTENEMY_STATES.RUNFAST)
			return;

		m_currentState = MTENEMY_STATES.RUNFAST;
	}

	public void VerticalLimiter()
	{
		if (this.transform.localPosition.y > -1.5f) {
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (this.GetComponent<Rigidbody2D> ().velocity.x, 0, 0);
		}
		if (this.transform.localPosition.y <= -5f) {
			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (this.GetComponent<Rigidbody2D> ().velocity.x, 0, 0);
		}
	}

	public void MoveLeft()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( -m_moveSpeed * m_moveSpeedMultiplier, m_moveSpeedMultiplierVertical, 0);
		this.transform.localScale = new Vector3 (m_currentScale, m_currentScale, m_currentScale);
		m_textChant.gameObject.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		if (this.transform.localPosition.x < -5.6f && GameScene.instance.m_eState != GAME_STATE.RESULTS) {
			if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 2 || m_currentMode == 1) {
				if (this.transform.localPosition.x < -6.6f) {
					GameScene.instance.Die ();
				}
			} else {
				m_moveDirection = -1;
			}
		}
	}

	public void MoveRight()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector3 ( m_moveSpeed * m_moveSpeedMultiplier, m_moveSpeedMultiplierVertical, 0);
		this.transform.localScale = new Vector3 (-m_currentScale, m_currentScale, m_currentScale);
		m_textChant.gameObject.transform.localScale = new Vector3(-0.01f, 0.01f, 0.01f);
		if (this.transform.localPosition.x > 5.6f && GameScene.instance.m_eState != GAME_STATE.RESULTS) {
			if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 2 || m_currentMode == 1) {
				if (this.transform.localPosition.x > 6.6f) {
					GameScene.instance.Die ();
				}
			} else {
				m_moveDirection = 1;
			}
		}
	}

	public void Reset(int direction, float speed, float verticalSpeed)
	{
		m_moveSpeedMultiplier = speed;
		m_moveDirection = direction;
		m_currentState = MTENEMY_STATES.RUN;
		this.transform.localEulerAngles = Vector3.zero;

		m_moveSpeedMultiplierVertical = verticalSpeed;

		this.GetComponent<BoxCollider2D> ().isTrigger = false;
		this.GetComponent<Rigidbody2D> ().freezeRotation = true;
		this.GetComponent<Rigidbody2D> ().gravityScale = 0;

		m_textChant.gameObject.SetActive (true);
		if (m_objectHair) {
			m_objectHair.SetActive (false);
			m_objectHair = null;
		}


	}

	public void Die()
	{
		if (m_currentState == MTENEMY_STATES.DIE)
			return;


		this.GetComponent<BoxCollider2D> ().isTrigger = true;
		this.GetComponent<Rigidbody2D> ().freezeRotation = false;

		m_currentState = MTENEMY_STATES.DIE;

		m_textChant.gameObject.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioDie);
	}

	GameObject ballObject;

	void OnCollisionEnter2D(Collision2D coll) {

		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		
		if (coll.gameObject.tag == "Player") {
			
			Die ();

			/*if (MTCharacter.instance.m_currentMode == 3) {
				MTCharacter.instance.PoopDie ();
				MTCharacter.instance.AnimateDeathParticle (this.gameObject);
			}*/

			ballObject = coll.gameObject;
			m_objectHair = coll.gameObject;

			coll.transform.parent = this.gameObject.transform;
			coll.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			coll.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;


			/*LeanTween.scale (this.gameObject, Vector3.zero, 0.1f);
			LeanTween.cancel (ballObject);
			LeanTween.scale (ballObject, ballObject.transform.localScale + new Vector3(0.1f,0.1f,0.1f), 0.1f);
			LeanTween.move (this.gameObject, ballObject.transform.position, 0.2f);
			coll.gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
			LeanTween.delayedCall (0.3f, BallRoutine);*/

			int randomRange = Random.Range (0, 100);


			if (coll.gameObject.transform.position.x < this.gameObject.transform.position.x + 0.1f &&
			    coll.gameObject.transform.position.x > this.gameObject.transform.position.x - 0.1f) {
				GameScene.instance.HitHeadshot (ballObject.gameObject.transform.position + new Vector3(0,1,0));
				GameScene.instance.Score (2);

				if (randomRange > 60) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range(-200,200), 1300));
					this.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (3, 6);
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (600, 800));
				} else if (randomRange > 30) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range(-200,200), Random.Range(1000, 1300)));
					this.GetComponent<Rigidbody2D> ().gravityScale = 4;
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-600, -800));
				} else {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range(-200,200), Random.Range(1000, 1300)));
					this.GetComponent<Rigidbody2D> ().gravityScale = 2;
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (1000, 800));
				}

			} else {
				GameScene.instance.Hit (ballObject.gameObject.transform.position + new Vector3(0,1,0));
				GameScene.instance.Score (1);

				if (randomRange > 60) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 400));
					this.GetComponent<Rigidbody2D> ().gravityScale = 0.8f;
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-400, 400));
				} else if (randomRange > 30) {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, Random.Range(0, 400)));
					this.GetComponent<Rigidbody2D> ().gravityScale = 0.8f;
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-400, 400));
				} else {
					this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, Random.Range(0, 400)));
					this.GetComponent<Rigidbody2D> ().gravityScale = 0.8f;
					this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-400, 400));
				}
			}


		}
	}
	/*
	void BallRoutine()
	{
		this.gameObject.SetActive (false);
		//ballObject.gameObject.SetActive (false);
		LeanTween.alpha(ballObject, 0, 0.1f).setOnComplete(BallRoutine2);
	}

	void BallRoutine2()
	{
		ballObject.gameObject.SetActive (false);
		LeanTween.alpha (ballObject, 1, 0.01f);
	}*/

}
