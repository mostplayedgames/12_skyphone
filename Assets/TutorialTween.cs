﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTween : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LeanTween.moveY (this.gameObject, this.gameObject.transform.position.y - 0.8f, 0.3f).setLoopPingPong ();
		LeanTween.scale (this.gameObject, new Vector3 (0.2f, 0.16f, 0.2f), 0.3f).setLoopPingPong ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
