﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MAGNET_STATE
{
	MOVERIGHT,
	MOVELEFT
}

public enum MAGNET_GAMESTATE
{
	SPAWN,
	DROP,
	MOVEUP
}

public class MagnetObject : MonoBehaviour {

	public GameObject m_objectRootSpawn;
	public float m_magnetSpeed;

	public GameObject m_prefabStackObject;
	public GameObject m_prefabStackObjectPart;

	public GameObject m_objectCamera;

	public AudioClip m_audioScore;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioDrop;
	public AudioClip m_audioMinusHP;

	public TextMesh m_textMeshNumber;

	public GameObject m_objectMagnetArm;

	public GameObject m_objectTopObject;

	public List<int> m_listSpeedTuning;

	MAGNET_STATE m_eState;
	MAGNET_GAMESTATE m_eGameState;

	// Use this for initialization
	void Start () {
		m_eState = MAGNET_STATE.MOVERIGHT;

	}
	
	// Update is called once per frame
	void Update () {
		switch (m_eState) {
		case MAGNET_STATE.MOVERIGHT:
			//MoveRight ();
			break;
		case MAGNET_STATE.MOVELEFT:
			//MoveLeft ();
			break;
		}
	}

	int m_currentHorizontalPosition;
	float DEFAULT_X_MOVE = 4;
	bool isDropped = false;
	float m_currentHorizontalSpeed = 1;
	public GameObject m_movingObject;
	bool isHorizonal = false;
	bool isMovingArm = false;

	void MoveRight()
	{
		if (isDropped)
			return;
		
		//this.transform.localPosition += new Vector3 (m_magnetSpeed * Time.deltaTime, 0, 0);
		//this.transform.localPosition += new Vector3 (DEFAULT_X_MOVE, 0, 0);
		m_currentHorizontalPosition++;
		if( isHorizonal )
			this.transform.localPosition = new Vector3 (m_currentHorizontalPosition * DEFAULT_X_MOVE, this.transform.localPosition.y, -5.5f);
		else
			this.transform.localPosition = new Vector3 (0, this.transform.localPosition.y, (m_currentHorizontalPosition * DEFAULT_X_MOVE * 0.7f) - 5.5f );
		//if (this.transform.localPosition.x >= 9) {
		if (m_currentHorizontalPosition > 2) {
			m_eState = MAGNET_STATE.MOVELEFT;
			MoveLeftRoutine ();
			//if( GameScene.instance.GetScore() > 0 ){
			//	DecreaseHP ();
			//}

			ZAudioMgr.Instance.PlaySFXVolume (m_audioMinusHP, 0.25f);
		} else {
			MoveRightRoutine ();
		}
		//}
		//Debug.Log ("Move Right");
		if (m_currentHP <= 0) {
			//GameScene.instance.Die ();
			//isDropped = true;
			//TimesUpDie();
		}

		if( !GameScene.instance.isSplash )
		ZAudioMgr.Instance.PlaySFXVolume (m_audioDrop, 0.15f);
	}

	void ResetHPBar()
	{
		m_textMeshNumber.color = new Color (1, 1, 1, 1);
		LeanTween.cancel (m_textMeshNumber.gameObject);
		m_textMeshNumber.gameObject.SetActive (false);
	}

	void DecreaseHP()
	{
		if (GameScene.instance.GetScore () < 4)
			return;
		
		m_currentHP--;
		m_textMeshNumber.text = "" + m_currentHP; //
		if (m_currentHP <= 1) {
			m_textMeshNumber.color = new Color(1,0,0,1);
			LeanTween.cancel (m_textMeshNumber.gameObject);
			LeanTween.alpha (m_textMeshNumber.gameObject, 0.4f, 0.2f).setLoopPingPong ();
		}/*else if (m_currentHP <= 2) {
			m_textMeshNumber.color = new Color(1,;
			//LeanTween.alpha (m_textMeshNumber.gameObject, 0.4f, 0.5f).setLoopPingPong ();
		}*/
		else if (m_currentHP <= 2) {
			m_textMeshNumber.color = Color.yellow;
			LeanTween.cancel (m_textMeshNumber.gameObject);
		}else{
			m_textMeshNumber.color = Color.white;
			LeanTween.cancel (m_textMeshNumber.gameObject);
		}


		ZAudioMgr.Instance.PlaySFX (m_audioMinusHP);
	}

	void MoveRightRoutine()
	{
		if (isDropped)
			return;
		
		LeanTween.moveX (m_movingObject, 0, m_currentHorizontalSpeed).setOnComplete( MoveRight);
	}

	void MoveLeft()
	{
		if (isDropped)
			return;

		//Debug.Log ("Move Left");
		//this.transform.localPosition -= new Vector3 (m_magnetSpeed * Time.deltaTime, 0, 0);
		//this.transform.localPosition -= new Vector3 (DEFAULT_X_MOVE, 0, 0);
		m_currentHorizontalPosition--;
		if( isHorizonal )
			this.transform.localPosition = new Vector3 (m_currentHorizontalPosition * DEFAULT_X_MOVE, this.transform.localPosition.y, -5.5f);
		else
			this.transform.localPosition = new Vector3 (0, this.transform.localPosition.y, (m_currentHorizontalPosition * DEFAULT_X_MOVE * 0.7f) - 5.5f);
		//if (this.transform.localPosition.x <= -7) {
		if (m_currentHorizontalPosition < -2) {
			m_eState = MAGNET_STATE.MOVERIGHT;
			MoveRightRoutine ();
			//if( GameScene.instance.GetScore() > 0 ){
				//DecreaseHP ();
			//}
			ZAudioMgr.Instance.PlaySFXVolume (m_audioMinusHP, 0.25f);
		} else {
			MoveLeftRoutine ();
		}
		//}

		if (m_currentHP <= 0) {
			//GameScene.instance.Die ();
			//TimesUpDie();
		}

		if( !GameScene.instance.isSplash )
		ZAudioMgr.Instance.PlaySFXVolume (m_audioDrop, 0.15f);
	}

	void MoveLeftRoutine()
	{
		if (isDropped)
			return;
		
		LeanTween.moveX (m_movingObject, 0, m_currentHorizontalSpeed).setOnComplete( MoveLeft);
	}

	GameObject m_currentPart;

	public List<GameObject> m_listParts;

	int m_currentHighscore = 0;
	int m_currentLevel = 0;
	float m_currentLevelCounter = 0;
	int m_currentNumber = 0;
	int m_currentChips = 0;
	float m_currentScale = 1;

	int m_defaultSpeed = 7;
	//int m_minSpeed = 5;
	float m_currentSpeed;

	float m_currentHeight;

	int m_currentRhythmer;
	int m_currentDirectioner;
	int m_currentBeater;
	int m_currentHP;

	public void SpawnPart()
	{
		if (m_eGameState == MAGNET_GAMESTATE.SPAWN)
			return;
		
		//NextLevel ();
		//Debug.Log("SPawn Part");
		//m_currentPart = ZObjectMgr.Instance.Spawn2D (m_prefabStackObjectPart.name, m_objectRootSpawn.transform.position);
		m_currentPart = ZObjectMgr.Instance.Spawn2D (m_prefabStackObject.name, m_objectRootSpawn.transform.position);
		m_currentPart.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
		//Debug.LogError ("Chips : " + m_currentChips + " );
		// Fixed widths
		//Debug.Log("Spawn Current Scale : " + m_currentScale);
		/*m_currentPart.transform.localScale = new Vector3 (1.2f / (m_currentChips) * m_currentScale,// * 0.7f, 
			m_currentPart.transform.localScale.y,
			m_currentPart.transform.localScale.z);*/

		//Debug.Log ("Current Chips : " + m_currentChips);
		// Random Widths
		/*
		float currentScale = Random.Range(0.1f, m_currentScale);//Random.Range(0, m_currentScale/2f);
		currentScale = Mathf.Min(currentScale, 0.5f);

		if (m_currentNumber <= 0) {
			currentScale = m_currentScale;
		} else if (m_currentScale - currentScale <= 0) {
			currentScale = m_currentScale;
			m_currentScale = 0;
		} else {
			m_currentScale -= currentScale;
		}
		m_currentPart.transform.localScale = new Vector3 (currentScale, 
				m_currentPart.transform.localScale.y,
				m_currentPart.transform.localScale.z);
	*/

		m_currentPart.transform.position = m_objectRootSpawn.transform.position;
		m_currentPart.transform.parent = m_objectRootSpawn.transform;
		m_currentPart.transform.localEulerAngles = Vector3.zero;
		m_currentPart.GetComponent<Rigidbody> ().isKinematic = true;
		m_currentPart.GetComponent<Collider> ().enabled = false;
		m_currentPart.GetComponent<StackObject> ().isAlive = true;
		m_currentPart.GetComponent<StackObject> ().Reset ();
		//m_currentPart.GetComponent<StackObject> ().isCurrentLevel = false;
		//m_currentPart.GetComponent<StackObject> ().enabled = true;

		//m_textMeshNumber.text = "" + m_currentNumber;
		
		//m_listParts.Add (m_currentPart);

		if (GameScene.instance.GetScore () == -99) {
			m_textMeshNumber.gameObject.SetActive (true);
			m_textMeshNumber.color = new Color (1, 1, 1, 1);
			LeanTween.cancel (m_textMeshNumber.gameObject);
		}

		ZAudioMgr.Instance.PlaySFX (m_audioScore);


		m_eGameState = MAGNET_GAMESTATE.SPAWN;
	}

	public void NextLevel(bool isStart = false)
	{
		/*if (m_currentHorizontalPosition == 0) {
			GameScene.instance.Score (1);
		} else {
			GameScene.instance.Die ();
		}*/
		//if (m_currentNumber <= 0) {
		if (m_currentHorizontalPosition == 0) {
			//Debug.Log ("NExt Level");
			//m_currentLevel++;
			RandomLevel ();
			m_currentNumber = m_currentChips;

			if (isStart)
				m_objectMagnetArm.transform.localPosition = new Vector3 (m_objectMagnetArm.transform.localPosition.x, 
					15f, 
					m_objectMagnetArm.transform.localPosition.z);
			else
				MoveArm ();

			/*foreach (GameObject parts in m_listParts) {
				parts.SetActive (false);
				parts.GetComponent<StackObject> ().isAlive = false;
				parts.transform.parent = null;
			}
			m_listParts.Clear ();*/

			GameScene.instance.Hit(m_currentPart.transform.localPosition);

			/*if (!isStart) {
				GameObject wholeObject = ZObjectMgr.Instance.Spawn2D (m_prefabStackObject.name, m_objectRootSpawn.transform.position);
				wholeObject.transform.position = new Vector3 (0, m_currentHeight - 18, wholeObject.transform.localPosition.z);
				wholeObject.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
				wholeObject.GetComponent<Rigidbody> ().isKinematic = false;
				//wholeObject.GetComponent<StackObject> ().isAlive = false;
				//wholeObject.GetComponent<StackObject> ().isCurrentLevel = false;
				//wholeObject.GetComponent<StackObject> ().enabled = false;

				//GameScene.instance.Score (1);
				GameScene.instance.Score (1);
			}*/

			ZAudioMgr.Instance.PlaySFX (m_audioSuccess);

			m_currentHeight += 1.85f;
			//this.transform.localPosition = new Vector3 (this.transform.localPosition.x, m_currentHeight + 2, 0);

			LeanTween.moveLocal (m_objectCamera, new Vector3(m_objectCamera.transform.localPosition.x, 
				m_currentHeight, 
				m_objectCamera.transform.localPosition.z)
				, 0.15f);

			isMovingArm = false;
			ResetMagnetMove ();
			//m_currentScale = 1;
			//m_currentScale = Mathf.Max(0.3f + (0.05f * m_currentLevel), 0.95f);
			//m_currentScale = Random.Range(0.4f, Mathf.Max(0.4f, 1.0f - (m_currentLevel*0.1f)));
			//m_currentScale =  Random.Range(0.4f + (m_currentLevel*0.05f), Mathf.Max(0.4f, 1.2f - (m_currentLevel*0.05f)));
			//m_currentScale = 1.2f - (m_currentLevel*0.075f);//1.2f - (m_currentLevel*0.05f) - Random.Range(0, 0.3f);
			//if( GameScene.instance.GetLevel() >= 0.7f )
			int currentTempLevel = GameScene.instance.GetLevel();
			if (GameScene.instance.m_currentMode == 1) {
				currentTempLevel = m_currentLevel;
			}
			if (currentTempLevel >= 12)
				m_currentScale = 0.5f;
			else if (currentTempLevel >= 8)
				m_currentScale = 0.65f;
			else if (currentTempLevel >= 6)
				m_currentScale = 0.7f;
			else if (currentTempLevel >= 4)
				m_currentScale = 0.8f;
			else
				m_currentScale = 0.9f;
			//m_currentScale = Mathf.Max (0.4f, m_currentScale);
			m_currentScale *= Random.Range(0.8f, 1f);
			//Debug.Log ("SCALE : " + m_currentScale);
			SpawnPart ();

			GameScene.instance.Score (1);
			//m_magnetSpeed += 0.5f;

			//m_currentSpeed = m_defaultSpeed + (m_currentLevel*0.2f);
			RandomMagnetSpeed ();

			//m_textMeshNumber.text = "" + m_currentNumber;
		}else if(!isStart) {
			//Debug.Log ("Level " + m_currentHorizontalPosition + " Name : " + m_currentPart.gameObject.name);
			GameScene.instance.Die ();
			m_currentPart.GetComponent<Rigidbody> ().isKinematic = false;
			m_currentPart.GetComponent<Rigidbody> ().velocity = new Vector3 (0, Random.Range (10, 50), -Random.Range (5, 10));
			m_currentPart.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;
			//m_currentPart.GetComponent<Rigidbody> ().AddForce (0, Random.Range (0, 500), 0);
			m_currentPart.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range (-3000, 3000), Random.Range (-3000, 3000), Random.Range (-3000, 3000)));
		}
		
	}

	public void MoveArm()
	{
		LeanTween.cancel (m_objectMagnetArm);
		//LeanTween.moveLocal(m_objectMagnetArm, new Vector3(m_objectMagnetArm.transform.localPosition.x,
		//	//Random.Range(9.8f, 14.5f),
		//	10f,
		//	m_objectMagnetArm.transform.localPosition.z), 0.15f);
		m_objectMagnetArm.transform.localPosition = new Vector3 (m_objectMagnetArm.transform.localPosition.x,
			Random.Range(9.8f, 17.5f),
			//12f,
			m_objectMagnetArm.transform.localPosition.z);
	}

	void MoveUp()
	{
		GameObject wholeObject = ZObjectMgr.Instance.Spawn2D (m_prefabStackObject.name, m_objectRootSpawn.transform.position);
		wholeObject.transform.position = new Vector3 (0, wholeObject.transform.localPosition.y - 6f, wholeObject.transform.localPosition.z);
		wholeObject.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
		//wholeObject.GetComponent<StackObject> ().isAlive = false;
		//wholeObject.GetComponent<StackObject> ().isCurrentLevel = false;
		//wholeObject.GetComponent<StackObject> ().enabled = false;

		//GameScene.instance.Score (1);
		//ZAudioMgr.Instance.PlaySFX (m_audioSuccess);

		//this.transform.localPosition += new Vector3 (0, 1.8f, 0);
		m_objectCamera.transform.localPosition += new Vector3 (0, 1.8f, 0);
	}

	public void Drop()
	{
	if (m_eGameState == MAGNET_GAMESTATE.DROP)
			return;
	LeanTween.moveLocal(m_objectMagnetArm, new Vector3(m_objectMagnetArm.transform.localPosition.x,
		50f,
		m_objectMagnetArm.transform.localPosition.z), 0.2f);
	
		m_currentPart.GetComponent<Rigidbody> ().isKinematic = false;
		m_currentPart.GetComponent<Rigidbody> ().velocity = new Vector3 (0, -40, 0);
		m_currentPart.GetComponent<Collider> ().enabled = true;
		m_currentPart.transform.parent = null;
		m_currentNumber--;

		//if( m_currentNumber > 0 )
		//	SpawnPart ();
		//m_textMeshNumber.text = "" + m_currentNumber;

		RandomMagnetSpeed ();

		LeanTween.cancel (m_movingObject);

		ZAudioMgr.Instance.PlaySFX (m_audioDrop);

	//Debug.Log ("Drop " + m_currentHorizontalPosition);
		isDropped = true;

		m_eGameState = MAGNET_GAMESTATE.DROP;
	}

	int m_currentHorizontalSpeedIndex = 0;

	void ResetMagnetMove()
	{
		if (isMovingArm)
			return;

		isMovingArm = true;
		m_currentRhythmer--;
		
		if (Random.Range (0, 100) > 50) {
			isHorizonal = true;
		} else {
			isHorizonal = false;
		}

		isHorizonal = true;
		isDropped = false;
	//DEFAULT_X_MOVE = Random.Range (2, 6);

		// DAM TUNING
		if( GameScene.instance.GetScore() > 50 )
			DEFAULT_X_MOVE = Random.Range (0.35f, 2f);
		else if( GameScene.instance.GetScore() > 30 )
			DEFAULT_X_MOVE = Random.Range (0.45f, 2f);
		else if (GameScene.instance.GetScore () > 20)
			DEFAULT_X_MOVE = Random.Range (0.5f, 2f);
		else if (GameScene.instance.GetScore () > 10)
			DEFAULT_X_MOVE = Random.Range (1.5f, 2f);
		else
			DEFAULT_X_MOVE = Random.Range (1.5f, 1.5f);

		DEFAULT_X_MOVE = 4f;

		Debug.LogWarning ("Default X MOVE : " + DEFAULT_X_MOVE);

		//DEFAULT_X_MOVE = 3;
	//m_currentHorizontalSpeed = Random.Range (0.1f, 0.6f);
	//m_currentHorizontalSpeed = Random.Range (0.2f, 0.8f);
		/*if (GameScene.instance.GetScore () > 10) {
			m_currentHorizontalSpeed = Random.Range (0.2f, 0.6f);
		} else if (GameScene.instance.GetScore () > 5) {
			m_currentHorizontalSpeed = Random.Range (0.25f, 0.6f);
		} else {
			m_currentHorizontalSpeed = Random.Range (0.3f, 0.6f);
		}*/
		//m_currentHorizontalSpeed = 0.3f;
		//if( GameScene.instance.GetScore() % 5 == 0 )
		//	m_currentHorizontalSpeed -= 0.05f;

		//if (m_currentHorizontalSpeed >= 0.07) {
		//	m_currentHorizontalSpeed -= 0.01f;
		//}
		// dam tuning
		float tuning_coefficient = 0.65f - (0.10f * Mathf.Min(1, GameScene.instance.GetScore() / 20f));
		int tuningValue = 0;
		if (GameScene.instance.GetBestScore () > 10 && GameScene.instance.GetScore () < GameScene.instance.GetBestScore ()/2f) {
			int tuningValueRandom = Random.Range (0, 100);
			if (tuningValueRandom < 25) {
				tuningValue = 0;
			} else if (tuningValueRandom < 50) {
				tuningValue = 1;
			} else if (tuningValueRandom < 75) {
				tuningValue = 2;
			} else {
				tuningValue = 3;
			}
		} else {
			tuningValue = m_listSpeedTuning [m_currentHorizontalSpeedIndex % m_listSpeedTuning.Count];
		}
		switch (tuningValue) {
		case 0: m_currentHorizontalSpeed = Random.Range (0.65f, 0.7f) * tuning_coefficient;
			break;
		case 1: m_currentHorizontalSpeed = Random.Range (0.45f, 0.6f) * tuning_coefficient;
			break;
		case 2: m_currentHorizontalSpeed = Random.Range (0.24f, 0.3f) * tuning_coefficient;
			break;
		case 3: m_currentHorizontalSpeed = Random.Range (0.2f, 0.22f) * tuning_coefficient;
			break;
		case 4: m_currentHorizontalSpeed = Random.Range (0.15f, 0.155f) * tuning_coefficient;
			break;
		case 5: m_currentHorizontalSpeed = Random.Range (0.14f, 0.145f) * tuning_coefficient;
			break;
		}
		m_currentHorizontalSpeedIndex++;


		float TIME_SLOW = 1.8f;
		float TIME_MEDIUM = 1.4f;
		float TIME_FAST = 1f;

		// 7 To The Right
		// DAM TUNING
		if( m_currentDirectioner > 0 ){

			if (m_currentBeater > 0) 
			{
				m_currentHorizontalPosition = -5;
			}
			else 
			{
				if (Random.Range (0, 100) > 80) {
					m_currentHorizontalPosition = -6;
				} else if (Random.Range (0, 100) > 80 && GameScene.instance.GetScore() > 15) {
					m_currentHorizontalPosition = -7;
				}  else {
					m_currentHorizontalPosition = -4;
				}
			}
			m_currentHorizontalPosition = -4;
			MoveRight ();
			m_currentDirectioner = -1;
		} else {

			if (m_currentBeater > 0) 
			{
				m_currentHorizontalPosition = 5;
			}
			else {
				if (Random.Range (0, 100) > 80) {
					m_currentHorizontalPosition = 6;
				}  else if (Random.Range (0, 100) > 80 && GameScene.instance.GetScore() > 15) {
					m_currentHorizontalPosition = 7;
				}  else {
					m_currentHorizontalPosition = 4;
				}
			}
			m_currentHorizontalPosition = 4;
			MoveLeft ();
			m_currentDirectioner = 1;


		}

		Debug.LogWarning ("HOR POS : " + m_currentHorizontalPosition);
		//Debug.LogWarning ("BEATER : " + m_currentBeater);
		//DEFAULT_X_MOVE = Random.Range (2, 6);
		//DEFAULT_X_MOVE = 3;
		/*if (GameScene.instance.GetScore () <= 90) {
			m_currentHorizontalPosition = 0;
			isDropped = true;
			this.transform.localPosition = new Vector3 (m_currentHorizontalPosition * DEFAULT_X_MOVE, this.transform.localPosition.y, -5.5f);
		}*/

		m_currentBeater--;
		if (m_currentBeater < 0)
			SetupBeater ();


		
		//m_currentHorizontalSpeed = 0.1f;
	//m_currentHorizontalSpeed -= 0.02f;
	}

	void RandomLevel()
	{
		//m_currentChips = Random.Range (2, m_currentLevel);
		if (GameScene.instance.m_currentMode == 0) {
			m_currentChips = m_currentLevel - 1;
		} else {
			//m_currentChips = m_currentLevel + 1;
			/*if (m_currentLevel < m_currentHighscore) {
				m_currentChips = Random.Range (m_currentLevel + 1, Mathf.Max(2, Mathf.CeilToInt(m_currentHighscore/2f)));
				//Debug.Log ("Random Highscore : " + Mathf.Max(2, m_currentHighscore) + " Chips : " + m_currentChips + " Current Level : " + (m_currentLevel + 1));
			} else {
				m_currentChips = m_currentLevel + 1;
				//Debug.Log ("Random Highscore Old");
			}*/
			m_currentChips = m_currentLevel + 1;
		}
		m_currentLevelCounter++;
		if (m_currentLevelCounter % 3 == 0) {
			m_currentLevel++;
		}
	}

	void RandomMagnetSpeed()
	{
		if (GameScene.instance.m_currentMode == 0) {
			m_currentSpeed = m_defaultSpeed + (m_currentLevel * 0.2f);
			m_magnetSpeed = Random.Range (3, m_currentSpeed);
		} else {
			m_currentSpeed = m_defaultSpeed + (m_currentLevel * 0.2f);
			m_magnetSpeed = Random.Range (5, m_currentSpeed + 5);
		}

		//m_currentSpeed = 3;
		//m_magnetSpeed = 6;

		//Debug.Log ("Magnet Speed : " + m_magnetSpeed);
	}

	public void TimesUpDie()
	{
		isDropped = true;
		ZCameraMgr.instance.DoShake ();
		m_textMeshNumber.color = new Color (1, 0, 0, 1);
		Color color = m_textMeshNumber.GetComponent<Renderer>().material.color;
		color.a = 1.0f;
		m_textMeshNumber.GetComponent<Renderer>().material.color = color;

		LeanTween.cancel (m_textMeshNumber.gameObject);

		LeanTween.delayedCall (1.5f, TimesUpDieRoutine);
	}

	void TimesUpDieRoutine()
	{
		GameScene.instance.Die ();
	}

	public void Die()
	{
		isMovingArm = false;
		LeanTween.delayedCall (1.5f, DieRoutine);	

		//foreach (GameObject parts in m_listParts) {
			//parts.GetComponent<Rigidbody> ().isKinematic = false;
		//	parts.GetComponent<Rigidbody> ().AddForce (new Vector3 (Random.Range (500, 1500), Random.Range (500, 1500), Random.Range (500, 1500)));
			//Debug.Log ("Fly");
		//}

		LeanTween.moveLocal(m_objectMagnetArm, new Vector3(m_objectMagnetArm.transform.localPosition.x,
			m_objectMagnetArm.transform.localPosition.y + 50f,
			m_objectMagnetArm.transform.localPosition.z), 0.4f);
		//m_objectTopObject.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
		//m_objectTopObject.GetComponent<Rigidbody> ().isKinematic = false;
	}

	void DieRoutine()
	{
		ZAudioMgr.Instance.PlaySFX (m_audioDrop);
		m_objectMagnetArm.SetActive (false);
		m_objectTopObject.SetActive(true);
		m_objectTopObject.transform.localPosition = new Vector3 (0, m_currentHeight - 10f, m_objectTopObject.transform.localPosition.z);
		///m_objectTopObject.transform.position = new Vector3 (0, m_objectTopObject.transform.localPosition.y - 2f, m_objectTopObject.transform.localPosition.z);
		/*foreach (GameObject parts in m_listParts) {
			parts.SetActive (false);
			parts.GetComponent<StackObject> ().isAlive = false;
			parts.transform.parent = null;
		}*/
	}

	public void SetupBeater()
	{
		if(GameScene.instance.GetScore() > 10 )
			m_currentBeater = Random.Range (1, 4);
		else
			m_currentBeater = Random.Range (2, 4);
	}

	public void Reset()
	{
		
	LeanTween.cancel (m_movingObject);
		isHorizonal = true;
		//this.transform.localPosition = new Vector3 (0, -6.6f, 0);
		//this.transform.localPosition = new Vector3 (0, 7.6f, 0);
		//this.transform.localPosition = new Vector3 (0, 5.6f, 0);
		//this.transform.localPosition = new Vector3 (0, 0.6f, 0);
		//this.transform.localPosition = new Vector3 (0, -2.6f, 0);
		m_currentHeight = -4.6f - 1.8f;
		m_currentHorizontalSpeedIndex = 0;
		//m_currentLevel = 2;
		//m_currentLevel = 2;
		//m_currentLevel = 2 + GameScene.instance.GetLevel();
		if (GameScene.instance.m_currentMode == 0) {
			m_currentLevel = GameScene.instance.GetLevel ();
			for (int x = 0; x < GameScene.instance.GetLevel (); x++) {
				MoveUp ();
			}
			m_currentNumber = 0;
		} else {
			m_currentLevel = 1;
			m_currentLevelCounter = 1;
			m_currentNumber = 0;
			m_currentHighscore = GameScene.instance.GetLevel ();
		}

		//RandomLevel ();
		//m_currentLevelm_currentNumber = 0;
		m_objectTopObject.SetActive(false);

		m_currentScale = 1;
		m_textMeshNumber.color = new Color (1, 1, 1, 1);
		SetupBeater ();

		// DAM TUNING
		m_currentHP = 10;
		m_textMeshNumber.text = "" + m_currentHP; //

		//m_currentHorizontalPosition = Random.Range (-5, 5);
		//m_currentHorizontalPosition = 7;

		this.transform.localPosition = new Vector3 (0, 0, this.transform.localPosition.z);
		RandomMagnetSpeed ();
		//m_magnetSpeed = m_minSpeed;//Random.Range (m_minSpeed, m_maxSpeed);

		m_currentHorizontalSpeed = 0.3f;

		m_eGameState = MAGNET_GAMESTATE.DROP;
		SpawnPart ();
		NextLevel(true);
		m_objectMagnetArm.SetActive (true);
		m_objectCamera.gameObject.transform.localPosition = new Vector3 (14.1f, -4.6f, -20);
		LeanTween.cancel (m_objectCamera);

	//if (isStart)
		m_objectMagnetArm.transform.localPosition = new Vector3 (m_objectMagnetArm.transform.localPosition.x, 
			15f, 
			m_objectMagnetArm.transform.localPosition.z);

		/*if (Random.Range (0, 100) > 50) {
			m_currentHorizontalPosition = -4;
			MoveLeft ();
		} else {
			m_currentHorizontalPosition = 4;
			MoveRight ();
		}*/

		// DAM TUNING
		float tuning_coefficient = 0.65f;
		if( GameScene.instance.GetBestScore() > 30 )
			m_currentHorizontalSpeed = 0.34f * tuning_coefficient;
		else if( GameScene.instance.GetBestScore() > 15 )
			m_currentHorizontalSpeed = 0.36f * tuning_coefficient;
		else if( GameScene.instance.GetBestScore() > 10 )
			m_currentHorizontalSpeed = 0.38f * tuning_coefficient;
		else
			m_currentHorizontalSpeed = 0.4f * tuning_coefficient;


		//m_currentHorizontalSpeed = Random.Range (0.05f, 0.4f) * tuning_coefficient;
		//m_currentHorizontalSpeed = 0.15f * tuning_coefficient;

		//m_currentHorizontalSpeed = m_listSpeedTuning
		
		ResetMagnetMove ();
	
		if (Random.Range (0, 100) > 50) {
			m_currentDirectioner = 1;
		} else {
			m_currentDirectioner = -1;
		}

		ResetHPBar ();
	}

	void RandomRhythmer()
	{
		m_currentRhythmer = Random.Range (10, 20);
	}

}
